package com.example.bean;

public class Employee {
private String name, email, capability, skill;
private Integer oracleid, level;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getCapability() {
	return capability;
}
public void setCapability(String capability) {
	this.capability = capability;
}
public String getSkill() {
	return skill;
}
public void setSkill(String skill) {
	this.skill = skill;
}
public Integer getOracleid() {
	return oracleid;
}
public void setOracleid(Integer oracleid) {
	this.oracleid = oracleid;
}
public Integer getLevel() {
	return level;
}
public void setLevel(Integer level) {
	this.level = level;
}
@Override
public String toString() {
	return "Employee [name=" + name + ", email=" + email + ", capability=" + capability + ", skill=" + skill
			+ ", oracleid=" + oracleid + ", level=" + level + "]";
}

}
