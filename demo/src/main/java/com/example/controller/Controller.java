package com.example.controller;

import javax.annotation.PostConstruct;

import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.example.bean.*;

@org.springframework.stereotype.Controller
public class Controller {
	@GetMapping("/")
	public String home() {
		return "home";
	}

	@PostMapping("/sendData")
	public String sendData(Employee employee, Model model) {
		String url = "http://localhost:8080/api/addUser";
		System.out.println("adas");
		MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
		paramMap.add("name", employee.getName());
		paramMap.add("oracleid", employee.getOracleid());
		paramMap.add("email", employee.getEmail());
		paramMap.add("capability", employee.getCapability());
		paramMap.add("skill", employee.getSkill());
		paramMap.add("level", employee.getLevel());
		RestTemplate restTemplate = new RestTemplate();
		try {
			String res = restTemplate.postForObject(url, paramMap, String.class);
			if (res.equalsIgnoreCase("success")) {
				model.addAttribute("employee", employee);
				return "success";
			} else {
				model.addAttribute("error", "OracleId already registered");
				return "error";
			}
		} catch (Exception e) {
			model.addAttribute("error", "OracleId already registered");
			return "error";
		}
	}

	@GetMapping("/getData")
	public String getData(@RequestParam("oracleid") Integer oracleid, Model model) {
		System.out.println("oracel id " + oracleid);
		String url = "http://localhost:8080/api/getUser/" + oracleid;
		RestTemplate restTemplate = new RestTemplate();
		try {
			Employee employee = restTemplate.getForObject(url, Employee.class);
			if (employee.getOracleid() != -1) {
				model.addAttribute("employee", employee);
				return "success";
			} else {
				model.addAttribute("error", "Invalid OracleId");
				return "error";
			}
		} catch (Exception e) {
			model.addAttribute("error", "Invalid OracleId");
			return "error";
		}

	}

}
