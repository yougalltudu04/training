<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Form</title>
  </head>
  <body>
  <div style="margin: auto; width: 50%; padding: 10px; background-color: #C4C1C0;" >
    <form action = "/demo/sendData" method="post">
    	<div class = "form-group">
    		<label>Name</label>
    		<input type="text" name="name" placeholder="Name" class="form-contorl" style="margin-left:10px; position: "><br>
    	</div>
    	<div class = "form-group">
    		<label>Oracle Id</label>
    		<input type="number" name="oracleid" placeholder="OracleId" class="form-contorl" style="margin-left: 10px"><br>
    	</div>
    	<div class = "form-group">
    		<label>Email</label>
    		<input type="email" name="email" placeholder="Email" class="form-contorl" style="margin-left: 10px"><br>
    	</div>
    	<div class = "form-group">
    		<label>Capability</label>
    		<input type="text" name="capability" placeholder="Capability" class="form-contorl" style="margin-left: 10px"><br>
    	</div>
    	<div class = "form-group">
    		<label>Skill</label>
    		<input type="text" name="skill" placeholder="Skill" class="form-contorl" style="margin-left: 10px"><br>
    	</div>
    	<div class = "form-group">
    		<label>Level</label>
    		<input type="number" min="0" max="5" name="level" placeholder="Level" class="form-contorl" style="margin-left: 10px"><br>
    	</div>
    	<input type="submit" value="Submit">
    </form><br>
    <h2>Search User</h2><br>
    <form action="/demo/getData" method="get">
    	<label>OracleId</label>
    	<input type="number" placeholder="OracleId" name="oracleid">
    	<input type="submit" value="Submit">
    </form>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>